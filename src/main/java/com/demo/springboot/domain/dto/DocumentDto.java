package com.demo.springboot.domain.dto;

public class DocumentDto {
    private String mark;
    private String player;
    private int X, Y;

    public DocumentDto() {
    }

    public DocumentDto(int x, int y, String player){
        this.player = player;
        this.X = x;
        this.Y = y;
    }

    public DocumentDto(String mark) {
        this.mark = mark;
    }

    public String getMark() {
        return mark;
    }

    public String getPlayer() {
        return player;
    }

    public int getX() {
        return X;
    }

    public int getY() {
        return Y;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public void setX(int x) {
        this.X = x;
    }

    public void setY(int y) {
        this.Y = y;
    }
}
