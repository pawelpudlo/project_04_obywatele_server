package com.demo.springboot.domain.dto;

import java.io.File;

public class GameDto {

    private String result;
    private String win;

    public GameDto(String result,String win) {
        this.result = result;
        this.win = win;
    }


    public String getJson() {
        return result;
    }

    public String getWin() {
        return win;
    }
}
