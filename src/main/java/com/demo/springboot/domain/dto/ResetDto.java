package com.demo.springboot.domain.dto;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class ResetDto {

    public ResetDto() throws FileNotFoundException{
        reset();
    }

    void reset() throws FileNotFoundException {

        PrintWriter re = new PrintWriter("game.txt");
        PrintWriter reai = new PrintWriter("ai.txt");
        PrintWriter win = new PrintWriter("win.txt");

            for(int i=0; i<9;i++) {
                re.println("-");
            }

            for(int i=0; i<2; i++){
                reai.println("-");
            }

            win.println("-");

            re.close();
            reai.close();
            win.close();
    }
}
