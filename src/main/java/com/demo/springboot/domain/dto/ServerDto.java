package com.demo.springboot.domain.dto;

public class ServerDto {

    private String X;
    private String O;

    public ServerDto(String x, String o) {
        X = x;
        O = o;
    }

    public void setX(String x) {
        X = x;
    }

    public void setO(String o) {
        O = o;
    }

    public String getX() {
        return "x";
    }

    public String getO() {
        return "o";
    }
}
