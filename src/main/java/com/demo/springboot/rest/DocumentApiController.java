package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.GameDto;
import com.demo.springboot.domain.dto.ResetDto;
import com.demo.springboot.service.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileNotFoundException;

@RestController
@RequestMapping(value = "/tictactoe")
    public class DocumentApiController {


        private static final Logger LOGGER = LoggerFactory.getLogger(DocumentApiController.class);


        @RequestMapping(value = "/result", method = RequestMethod.GET)
        public ResponseEntity<String> testDocument() throws FileNotFoundException {

            String result = gameService.result();
            String win = gameService.win();
            final GameDto game = new GameDto(result,win);


            if(win.equals("Player") || win.equals("player") || win.equals("Computer") || win.equals("Computer")){
                LOGGER.info("win: {win}",win);
                return new ResponseEntity<>(game.getJson() + "\nwin: " + game.getWin(), HttpStatus.OK);
            }else{
                LOGGER.info("Game state:");
                return new ResponseEntity<>(game.getJson(), HttpStatus.OK);
            }

        }



        @RequestMapping(value = "/reset-game", method = RequestMethod.POST)
        public ResponseEntity<ResetDto> tD() throws FileNotFoundException {

            final ResetDto reset = new ResetDto();
            LOGGER.info("Reset Game");
            return new ResponseEntity<>(HttpStatus.OK);
        }

        @Autowired
        private GameService gameService;

        @RequestMapping(value="/set-field-by-user", method=RequestMethod.POST)
        public ResponseEntity<Void> testDocument(@RequestParam(value="X", required=false) Integer X,
                                                 @RequestParam(value="Y", required = false) Integer Y,
                                                 @RequestParam(value="mark", required=false) String mark) throws FileNotFoundException{
            LOGGER.info("Set position for {}", mark);

            final int status = gameService.forGame(X,Y,mark);
            LOGGER.info("{}",status);
            if(status == 1){
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }else{
                return new ResponseEntity<>(HttpStatus.OK);
            }
        }

        @CrossOrigin
        @RequestMapping(value = "/set-field-by-ai", method = RequestMethod.POST)
        public ResponseEntity<Void> zwrot(@RequestParam(value="mark", required=false) String mark) throws FileNotFoundException {
            LOGGER.info("Set mark for computer");

            final int status = gameService.forAi(mark);
            LOGGER.info("{}",status);
            if(status == 1){
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }else{
                return new ResponseEntity<>(HttpStatus.OK);
            }
        }
}

