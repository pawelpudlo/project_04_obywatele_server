package com.demo.springboot.service;


import java.io.File;
import java.io.FileNotFoundException;

public interface GameService{



    int forGame(int X,int Y,String mark) throws FileNotFoundException;

    String result() throws FileNotFoundException;

    int forAi(String mark) throws FileNotFoundException;

    String win() throws FileNotFoundException;

}
