package com.demo.springboot.service.impl;


import com.demo.springboot.domain.dto.GameDto;
import com.demo.springboot.service.GameService;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;

@Service
public class GameServiceImpl implements GameService {

    public GameServiceImpl() {
    }

    @Override
    public int forGame(int X,int Y, String mark) throws FileNotFoundException{

        String[][] table = new String[3][3];
        String[] tab2 = new String[4];
        String computer;
        String[] ai = new String[2];
        String winner;
        int help;




        if(mark.equals("x") || mark.equals("X")){
            computer = "O";
        }else{
            computer = "X";
        }


        File aifile = new File ("ai.txt");
        File plik = new File("game.txt");
        File plik2 = new File("a.txt");
        File win0 = new File("win.txt");
        try{
            Scanner ride = new Scanner(plik);
            Scanner ride2 = new Scanner(plik2);
            Scanner air = new Scanner(aifile);
            Scanner winer = new Scanner(win0);

            winner = winer.nextLine();
            winer.close();
            if(winner.equals("Player") || winner.equals("player") || winner.equals("Computer") ||winner.equals("computer")){
                    return 1;
            }

            for(int i=0; i<3;i++){
                for(int j=0; j<3; j++){
                    table[i][j] = ride.nextLine();
                }
            }
            ride.close();

            for(int i=0; i<4;i++){
                tab2[i] = ride2.nextLine();
            }
            ride2.close();

            for(int i=0; i<2;i++){
                ai[i] = air.nextLine();
            }

            if(mark.equals(ai[0]) || mark.equals(ai[1])){
                return 1;
            }
            air.close();

        }catch(FileNotFoundException f){
            return 1;
        }

        if(table[X][Y].equals(tab2[0]) || table[X][Y].equals(tab2[1]) || table[X][Y].equals(tab2[2]) || table[X][Y].equals(tab2[3])){
            return 1;
        }else{
            if(mark.equals(tab2[0]) || mark.equals(tab2[1]) || mark.equals(tab2[2]) || mark.equals(tab2[3])){

                PrintWriter re = new PrintWriter("game.txt");


                for(int i=0; i<=2;i++){
                    for(int j=0; j<=2;j++){
                        if(i==X && j==Y){
                            re.println(mark);
                        }else{
                            re.println(table[i][j]);

                        }
                    }
                }

                table[X][Y] = mark;
                re.close();

                PrintWriter win = new PrintWriter("win.txt");

                if((table[0][0].equals("X") || table[0][0].equals("x")) && (table[0][1].equals("X") || table[0][1].equals("x")) && (table[0][2].equals("X") || table[0][2].equals("x"))){
                    if(mark.equals("X") || mark.equals("x")){
                        win.println("Player");
                        win.close();
                        return 2;
                    }else{
                        win.println("Computer");
                        win.close();
                        return 3;
                    }
                }else if((table[0][0].equals("O") || table[0][0].equals("o")) && (table[0][1].equals("O") || table[0][1].equals("o")) && (table[0][2].equals("O") || table[0][2].equals("o"))){
                    if(mark.equals("O") || mark.equals("o")){
                        win.println("Player");
                        win.close();
                        return 2;
                    }else{
                        win.println("Computer");
                        win.close();
                        return 3;
                    }
                }else if((table[0][0].equals("X") || table[0][0].equals("x")) && (table[1][0].equals("X") || table[1][0].equals("x")) && (table[2][0].equals("X") || table[2][0].equals("x"))){
                    if(mark.equals("X") || mark.equals("x")){
                        win.println("Player");
                        win.close();
                        return 2;
                    }else{
                        win.println("Computer");
                        win.close();
                        return 3;
                    }
                }else if((table[0][0].equals("O") || table[0][0].equals("o")) && (table[1][0].equals("O") || table[1][0].equals("o")) && (table[2][0].equals("O") || table[2][0].equals("o"))){
                    if(mark.equals("O") || mark.equals("o")){
                        win.println("Player");
                        win.close();
                        return 2;
                    }else{
                        win.println("Computer");
                        win.close();
                        return 3;
                    }
                }else if((table[0][0].equals("X") || table[0][0].equals("x")) && (table[1][1].equals("X") || table[1][1].equals("x")) && (table[2][2].equals("X") || table[2][2].equals("x"))){
                    if(mark.equals("X") || mark.equals("x")){
                        win.println("Player");
                        win.close();
                        return 2;
                    }else{
                        win.println("Computer");
                        win.close();
                        return 3;
                    }
                }else if((table[0][0].equals("O") || table[0][0].equals("o")) && (table[1][1].equals("O") || table[1][1].equals("o")) && (table[2][2].equals("O") || table[2][2].equals("o"))){
                    if(mark.equals("O") || mark.equals("o")){
                        win.println("Player");
                        win.close();
                        return 2;
                    }else{
                        win.println("Computer");
                        win.close();
                        return 3;
                    }
                }else if((table[0][1].equals("X") || table[0][1].equals("x")) && (table[1][1].equals("X") || table[1][1].equals("x")) && (table[2][1].equals("X") || table[2][1].equals("x"))){
                    if(mark.equals("X") || mark.equals("x")){
                        win.println("Player");
                        win.close();
                        return 2;
                    }else{
                        win.println("Computer");
                        win.close();
                        return 3;
                    }
                }else if((table[0][1].equals("O") || table[0][1].equals("o")) && (table[1][1].equals("O") || table[1][1].equals("o")) && (table[2][1].equals("O") || table[2][1].equals("o"))){
                    if(mark.equals("O") || mark.equals("o")){
                        win.println("Player");
                        win.close();
                        return 2;
                    }else{
                        win.println("Computer");
                        win.close();
                        return 3;
                    }
                }else if((table[1][0].equals("X") || table[1][0].equals("x")) && (table[1][1].equals("X") || table[1][1].equals("x")) && (table[1][2].equals("X") || table[1][2].equals("x"))){
                    if(mark.equals("X") || mark.equals("x")){
                        win.println("Player");
                        win.close();
                        return 2;
                    }else{
                        win.println("Computer");
                        win.close();
                        return 3;
                    }
                }else if((table[1][0].equals("O") || table[1][0].equals("o")) && (table[1][1].equals("O") || table[1][1].equals("o")) && (table[1][2].equals("O") || table[1][2].equals("o"))){
                    if(mark.equals("O") || mark.equals("o")){
                        win.println("Player");
                        win.close();
                        return 2;
                    }else{
                        win.println("Computer");
                        win.close();
                        return 3;
                    }
                }else if((table[0][2].equals("X") || table[0][2].equals("x")) && (table[1][1].equals("X") || table[1][1].equals("x")) && (table[2][0].equals("X") || table[2][0].equals("x"))){
                    if(mark.equals("X") || mark.equals("x")){
                        win.println("Player");win.close();
                        return 2;
                    }else{
                        win.println("Computer");win.close();
                        return 3;
                    }
                }else if((table[0][2].equals("O") || table[0][2].equals("o")) && (table[1][1].equals("O") || table[1][1].equals("o")) && (table[2][0].equals("O") || table[2][0].equals("o"))){
                    if(mark.equals("O") || mark.equals("o")){
                        win.println("Player");win.close();
                        return 2;
                    }else{
                        win.println("Computer");win.close();
                        return 3;
                    }
                }else if((table[0][2].equals("X") || table[0][2].equals("x")) && (table[1][2].equals("X") || table[1][2].equals("x")) && (table[2][2].equals("X") || table[2][2].equals("x"))){
                    if(mark.equals("X") || mark.equals("x")){
                        win.println("Player");win.close();
                        return 2;
                    }else{
                        win.println("Computer");win.close();
                        return 3;
                    }
                }else if((table[0][2].equals("O") || table[0][2].equals("o")) && (table[1][2].equals("O") || table[1][2].equals("o")) && (table[2][2].equals("O") || table[2][2].equals("o"))){
                    if(mark.equals("O") || mark.equals("o")){
                        win.println("Player");win.close();
                        return 2;
                    }else{
                        win.println("Computer");win.close();
                        return 3;
                    }
                }else if((table[2][0].equals("X") || table[2][0].equals("x")) && (table[2][1].equals("X") || table[2][1].equals("x")) && (table[2][2].equals("X") || table[2][2].equals("x"))){
                    if(mark.equals("X") || mark.equals("x")){
                        win.println("Player");win.close();
                        return 2;
                    }else{
                        win.println("Computer");win.close();
                        return 3;
                    }
                }else if((table[2][0].equals("O") || table[2][0].equals("o")) && (table[2][1].equals("O") || table[2][1].equals("o")) && (table[2][2].equals("O") || table[2][2].equals("o"))){
                    if(mark.equals("O") || mark.equals("o")){
                        win.println("Player");win.close();
                        return 2;
                    }else{
                        win.println("Computer");win.close();
                        return 3;
                    }
                }else{
                    win.println("-");
                    win.close();
                }



                Random randomPosition = new Random();
                int n,m;
                PrintWriter re2 = new PrintWriter("game.txt");

                do{
                    n = randomPosition.nextInt(3);
                    m = randomPosition.nextInt(3);
                    if(table[n][m].equals(tab2[0]) || table[n][m].equals(tab2[1]) || table[n][m].equals(tab2[2]) || table[n][m].equals(tab2[3])){
                        help =0;
                    }else{
                        for (int i = 0; i <= 2; i++) {
                            for (int j = 0; j <= 2; j++) {
                                if (i == n && j == m) {
                                    re2.println(computer);
                                } else {
                                    re2.println(table[i][j]);

                                }
                            }

                        }

                        PrintWriter aiw = new PrintWriter("ai.txt");
                            if(computer.equals("X") || computer.equals("x")){
                                aiw.println("X");
                                aiw.println("x");
                            }else if(computer.equals("O") || computer.equals("o")){
                                aiw.println("O");
                                aiw.println("o");
                            }
                        aiw.close();

                        table[n][m] = computer;
                        re2.close();

                        int x1=0,y1=0,zh=0;
                        for (int i = 0; i <= 2; i++) {
                            for (int j = 0; j <= 2; j++) {
                                if(table[i][j].equals(tab2[0]) || table[i][j].equals(tab2[1]) || table[i][j].equals(tab2[2]) || table[i][j].equals(tab2[3]) ){
                                }else{
                                    zh++;
                                    x1=i;
                                    y1=j;
                                }
                            }
                        }

                        if(zh == 1){
                            PrintWriter last = new PrintWriter("game.txt");
                            for (int i = 0; i <= 2; i++) {
                                for (int j = 0; j <= 2; j++) {
                                    if (i == x1 && j == y1) {
                                        last.println(mark);
                                    } else {
                                        last.println(table[i][j]);
                                    }
                                }
                            }
                            table[x1][y1] = mark;
                            last.close();
                        }

                        help = 1;
                    }
                }while(help == 0);

                PrintWriter win2 = new PrintWriter("win.txt");

                if((table[0][0].equals("X") || table[0][0].equals("x")) && (table[0][1].equals("X") || table[0][1].equals("x")) && (table[0][2].equals("X") || table[0][2].equals("x"))){
                    if(mark.equals("X") || mark.equals("x")){
                        win2.println("Player");win2.close();
                        return 2;
                    }else{
                        win2.println("Computer");win2.close();
                        return 3;
                    }
                }else if((table[0][0].equals("O") || table[0][0].equals("o")) && (table[0][1].equals("O") || table[0][1].equals("o")) && (table[0][2].equals("O") || table[0][2].equals("o"))){
                    if(mark.equals("O") || mark.equals("o")){
                        win2.println("Player");win2.close();
                        return 2;
                    }else{
                        win2.println("Computer");win2.close();
                        return 3;
                    }
                }else if((table[0][0].equals("X") || table[0][0].equals("x")) && (table[1][0].equals("X") || table[1][0].equals("x")) && (table[2][0].equals("X") || table[2][0].equals("x"))){
                    if(mark.equals("X") || mark.equals("x")){
                        win2.println("Player");win2.close();
                        return 2;
                    }else{
                        win2.println("Computer");win2.close();
                        return 3;
                    }
                }else if((table[0][0].equals("O") || table[0][0].equals("o")) && (table[1][0].equals("O") || table[1][0].equals("o")) && (table[2][0].equals("O") || table[2][0].equals("o"))){
                    if(mark.equals("O") || mark.equals("o")){
                        win2.println("Player");win2.close();
                        return 2;
                    }else{
                        win2.println("Computer");win2.close();
                        return 3;
                    }
                }else if((table[0][0].equals("X") || table[0][0].equals("x")) && (table[1][1].equals("X") || table[1][1].equals("x")) && (table[2][2].equals("X") || table[2][2].equals("x"))){
                    if(mark.equals("X") || mark.equals("x")){
                        win2.println("Player");win2.close();
                        return 2;
                    }else{
                        win2.println("Computer");win2.close();
                        return 3;
                    }
                }else if((table[0][0].equals("O") || table[0][0].equals("o")) && (table[1][1].equals("O") || table[1][1].equals("o")) && (table[2][2].equals("O") || table[2][2].equals("o"))){
                    if(mark.equals("O") || mark.equals("o")){
                        win2.println("Player");win2.close();
                        return 2;
                    }else{
                        win2.println("Computer");win2.close();
                        return 3;
                    }
                }else if((table[0][1].equals("X") || table[0][1].equals("x")) && (table[1][1].equals("X") || table[1][1].equals("x")) && (table[2][1].equals("X") || table[2][1].equals("x"))){
                    if(mark.equals("X") || mark.equals("x")){
                        win2.println("Player");win2.close();
                        return 2;
                    }else{
                        win2.println("Computer");win2.close();
                        return 3;
                    }
                }else if((table[0][1].equals("O") || table[0][1].equals("o")) && (table[1][1].equals("O") || table[1][1].equals("o")) && (table[2][1].equals("O") || table[2][1].equals("o"))){
                    if(mark.equals("O") || mark.equals("o")){
                        win2.println("Player");win2.close();
                        return 2;
                    }else{
                        win2.println("Computer");win2.close();
                        return 3;
                    }
                }else if((table[1][0].equals("X") || table[1][0].equals("x")) && (table[1][1].equals("X") || table[1][1].equals("x")) && (table[1][2].equals("X") || table[1][2].equals("x"))){
                    if(mark.equals("X") || mark.equals("x")){
                        win2.println("Player");win2.close();
                        return 2;
                    }else{
                        win2.println("Computer");win2.close();
                        return 3;
                    }
                }else if((table[1][0].equals("O") || table[1][0].equals("o")) && (table[1][1].equals("O") || table[1][1].equals("o")) && (table[1][2].equals("O") || table[1][2].equals("o"))){
                    if(mark.equals("O") || mark.equals("o")){
                        win2.println("Player");win2.close();
                        return 2;
                    }else{
                        win2.println("Computer");win2.close();
                        return 3;
                    }
                }else if((table[0][2].equals("X") || table[0][2].equals("x")) && (table[1][1].equals("X") || table[1][1].equals("x")) && (table[2][0].equals("X") || table[2][0].equals("x"))){
                    if(mark.equals("X") || mark.equals("x")){
                        win2.println("Player");win2.close();
                        return 2;
                    }else{
                        win2.println("Computer");win2.close();
                        return 3;
                    }
                }else if((table[0][2].equals("O") || table[0][2].equals("o")) && (table[1][1].equals("O") || table[1][1].equals("o")) && (table[2][0].equals("O") || table[2][0].equals("o"))){
                    if(mark.equals("O") || mark.equals("o")){
                        win2.println("Player");win2.close();
                        return 2;
                    }else{
                        win2.println("Computer");win2.close();
                        return 3;
                    }
                }else if((table[0][2].equals("X") || table[0][2].equals("x")) && (table[1][2].equals("X") || table[1][2].equals("x")) && (table[2][2].equals("X") || table[2][2].equals("x"))){
                    if(mark.equals("X") || mark.equals("x")){
                        win2.println("Player");win2.close();
                        return 2;
                    }else{
                        win2.println("Computer");win2.close();
                        return 3;
                    }
                }else if((table[0][2].equals("O") || table[0][2].equals("o")) && (table[1][2].equals("O") || table[1][2].equals("o")) && (table[2][2].equals("O") || table[2][2].equals("o"))){
                    if(mark.equals("O") || mark.equals("o")){
                        win2.println("Player");win2.close();
                        return 2;
                    }else{
                        win2.println("Computer");win2.close();
                        return 3;
                    }
                }else if((table[2][0].equals("X") || table[2][0].equals("x")) && (table[2][1].equals("X") || table[2][1].equals("x")) && (table[2][2].equals("X") || table[2][2].equals("x"))){
                    if(mark.equals("X") || mark.equals("x")){
                        win2.println("Player");win2.close();
                        return 2;
                    }else{
                        win2.println("Computer");win2.close();
                        return 3;
                    }
                }else if((table[2][0].equals("O") || table[2][0].equals("o")) && (table[2][1].equals("O") || table[2][1].equals("o")) && (table[2][2].equals("O") || table[2][2].equals("o"))){
                    if(mark.equals("O") || mark.equals("o")){
                        win2.println("Player");win2.close();
                        return 2;
                    }else{
                        win2.println("Computer");win2.close();
                        return 3;
                    }
                }else{
                    win2.println("-");
                    win2.close();
                }


                return 0;
            }else{
                return 1;
            }
        }
    }

    public String result() throws FileNotFoundException{

        String[][] table = new String[3][3];
        StringBuilder resultB = new StringBuilder();
        String result;
        String error = "Not found file with game status";

        File plik = new File("game.txt");

        try{
            Scanner ride = new Scanner(plik);
            for(int i=0; i<3;i++){
                for(int j=0; j<3; j++){
                    table[i][j] = ride.nextLine();
                }
            }

            ride.close();

            for(int f = 0; f<3; f++){
                for(int j=0; j<3; j++){
                    resultB.append(table[f][j]);
                    resultB.append(" ");
                }
                resultB.append("\n");
            }

            result = resultB.toString();
            return result;


        }catch(FileNotFoundException f1){
            return error;
        }

    }

    public int forAi(String mark) throws FileNotFoundException{

         String[] ai = {"X","O","x","o"};
         String[] zh = new String[2];
         int zh2;

        File aifile = new File ("ai.txt");

         try{

             Scanner air = new Scanner(aifile);

             for(int i=0; i<2;i++){
                 zh[i] = air.nextLine();
             }

             air.close();

             if(zh[0].equals(ai[0]) || zh[0].equals(ai[1]) || zh[0].equals(ai[2]) || zh[0].equals(ai[3])){
                 return 1;
             }
             if(zh[1].equals(ai[0]) || zh[1].equals(ai[1]) || zh[1].equals(ai[2]) || zh[1].equals(ai[3])){
                 return 1;
             }

         }catch(FileNotFoundException f){
             return 1;
         }


        if(mark.equals(ai[0]) || mark.equals(ai[2])){
            PrintWriter write = new PrintWriter("game.txt");
            PrintWriter writeai = new PrintWriter("ai.txt");
            Random rp = new Random();
            zh2 = rp.nextInt(9);

            for(int i=0;i<9;i++){
                if(i == zh2){
                    write.println(mark);
                }else{
                    write.println("-");
                }
            }

            writeai.println(ai[0]);
            writeai.println(ai[2]);
            writeai.close();
            write.close();
            return 0;
        }else if(mark.equals(ai[1]) || mark.equals(ai[3])){
            PrintWriter writeai = new PrintWriter("ai.txt");
            writeai.println(ai[1]);
            writeai.println(ai[3]);

            writeai.close();
            return 0;
        }else{
            return 1;
        }

    }

    public String win() throws FileNotFoundException{
        File win = new File("win.txt");

        try{
            Scanner win2 = new Scanner(win);
            String a = win2.nextLine();

            if(a.equals("Player") || a.equals("player") || a.equals("Computer") || a.equals("computer")){
                win2.close();
                return a;
            }

            win2.close();
        }catch(FileNotFoundException f1){
            return "Not find file please reset game";
        }

        return "";
    }
}
